EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ant_hardware-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "KaranaBaseUno"
Date ""
Rev "A"
Comp "Microenergy International"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4850 2550 2    60   ~ 0
D1
Text Label 4850 2650 2    60   ~ 0
D2
Text Label 4850 2750 2    60   ~ 0
D3
Text Label 4850 2850 2    60   ~ 0
D4
Text Label 4850 2950 2    60   ~ 0
D5
Text Label 4850 2450 2    60   ~ 0
D0
Text Label 4850 3050 2    60   ~ 0
D6
Text Label 4850 3150 2    60   ~ 0
D7
$Comp
L ATMEGA328P-A IC1
U 1 1 585BE770
P 2950 1950
F 0 "IC1" H 2200 3200 50  0000 L BNN
F 1 "328P" H 3350 550 50  0000 L BNN
F 2 "Housings_QFP:TQFP-32_7x7mm_Pitch0.8mm" H 2950 1950 50  0001 C CIN
F 3 "" H 2950 1950 50  0000 C CNN
F 4 "http://www.mouser.de/ProductDetail/Microchip-Technology-Atmel/ATMEGA328P-AU/?qs=sGAEpiMZZMu9ReDVvI6axzXTfdp%252beU5q9uBRrP4gYK0%3d" H 2950 1950 60  0001 C CNN "Mouser"
F 5 "1.78" H 2950 1950 60  0001 C CNN "Price"
F 6 "ATMEGA328P-AU" H 2950 1950 60  0001 C CNN "manf#"
	1    2950 1950
	1    0    0    -1  
$EndComp
Text Label 4800 1700 2    60   ~ 0
A0
Text Label 4800 1900 2    60   ~ 0
A2
Text Label 4800 1800 2    60   ~ 0
A1
Text Label 4800 2000 2    60   ~ 0
A3
Text Label 4800 2100 2    60   ~ 0
A4
Text Label 4800 2200 2    60   ~ 0
A5
Text Label 4800 950  2    60   ~ 0
D9
Text Label 4800 1050 2    60   ~ 0
D10
Text Label 4800 1150 2    60   ~ 0
D11
Text Label 4800 1250 2    60   ~ 0
D12
Text Label 4800 1350 2    60   ~ 0
D13
Text Label 4800 850  2    60   ~ 0
D8
Text Label 3950 1150 0    60   ~ 0
MOSI
Text Label 3950 1050 0    60   ~ 0
SS
Text Label 4150 2300 0    60   ~ 0
uC_RST
Text Label 3950 2450 0    60   ~ 0
uC_RX
Text Label 3950 2550 0    60   ~ 0
uC_TX
Text Label 3950 1450 0    60   ~ 0
XTAL1
Text Label 3950 1550 0    60   ~ 0
XTAL2
Text Label 1800 3050 0    60   ~ 0
GND
Text Label 1800 1150 2    60   ~ 0
5V
Text Label 1800 1450 2    60   ~ 0
5V
Text Notes 2100 1100 0    60   ~ 0
avcc <-> vcc?
$Comp
L GND #PWR018
U 1 1 585D8827
P 1800 3050
F 0 "#PWR018" H 1800 2800 50  0001 C CNN
F 1 "GND" H 1800 2900 50  0000 C CNN
F 2 "" H 1800 3050 50  0000 C CNN
F 3 "" H 1800 3050 50  0000 C CNN
	1    1800 3050
	1    0    0    -1  
$EndComp
Text Label 2050 2200 2    60   ~ 0
A6
Text Label 2050 2300 2    60   ~ 0
A7
Text Notes 2650 2650 1    60   ~ 0
this package of\nthe µC has 2 ADCs\nmore!
Text Label 1800 2200 2    60   ~ 0
V_gas
Text Label 1800 2300 2    60   ~ 0
V_temp
$Comp
L CONN_01X06 P3
U 1 1 5869B46A
P 3300 4650
F 0 "P3" H 3300 5000 50  0000 C CNN
F 1 "ftdi port" V 3400 4650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3300 4650 50  0001 C CNN
F 3 "" H 3300 4650 50  0000 C CNN
F 4 "0.15" H 3300 4650 60  0001 C CNN "Price"
F 5 "https://www.adafruit.com/product/598" H 3300 4650 60  0001 C CNN "Link"
	1    3300 4650
	1    0    0    -1  
$EndComp
Text Notes 3450 4900 0    60   ~ 0
dtr\nrx\ntx\nvcc\ncts\ngnd
Text Notes 2750 5150 0    60   ~ 0
ftdi connector\n for easy flashing
Text Label 2900 4500 2    60   ~ 0
uC_TX
Text Label 2900 4600 2    60   ~ 0
uC_RX
Text Label 2900 4700 2    60   ~ 0
5V
Text Label 2900 4900 2    60   ~ 0
GND
Text Notes 2250 3950 0    60   ~ 0
flashing
Text Notes 6200 650  0    60   ~ 0
power management
Text Label 9800 2000 0    60   ~ 0
GND
Text Label 8600 2000 2    60   ~ 0
uC_RST
NoConn ~ 3100 4800
NoConn ~ 3100 4400
$Comp
L C C2
U 1 1 589174ED
P 1850 1650
F 0 "C2" H 1875 1750 50  0000 L CNN
F 1 "100n" H 1875 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" V 1888 1500 50  0001 C CNN
F 3 "" H 1850 1650 50  0000 C CNN
	1    1850 1650
	1    0    0    -1  
$EndComp
Text Label 1850 1800 2    60   ~ 0
GND
$Comp
L R R7
U 1 1 58918847
P 8600 1600
F 0 "R7" V 8680 1600 50  0000 C CNN
F 1 "4.7k" V 8600 1600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8530 1600 50  0001 C CNN
F 3 "" H 8600 1600 50  0000 C CNN
	1    8600 1600
	1    0    0    -1  
$EndComp
Text Label 8600 1750 0    60   ~ 0
uC_RST
Text Label 8600 1450 0    60   ~ 0
5V
$Comp
L R R9
U 1 1 5891A334
P 8750 2000
F 0 "R9" V 8830 2000 50  0000 C CNN
F 1 "330" V 8750 2000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8680 2000 50  0001 C CNN
F 3 "" H 8750 2000 50  0000 C CNN
	1    8750 2000
	0    1    1    0   
$EndComp
$Comp
L Crystal Y1
U 1 1 5891E12D
P 6650 1850
F 0 "Y1" H 6650 2000 50  0000 C CNN
F 1 "16MHz" H 6650 1700 50  0000 C CNN
F 2 "Crystals:Crystal_HC49-4H_Vertical" H 6650 1850 50  0001 C CNN
F 3 "" H 6650 1850 50  0000 C CNN
F 4 "http://www.mouser.de/ProductDetail/IQD/LFXTAL003240Bulk/?qs=sGAEpiMZZMsBj6bBr9Q9af1kE%252bXo19x3XdNBGSHQEKo%252byGjE%2fWZ8lg%3d%3d" H 6650 1850 60  0001 C CNN "Mouser"
F 5 "0.153" H 6650 1850 60  0001 C CNN "Price"
F 6 "LFXTAL003240Bulk" H 6650 1850 60  0001 C CNN "manf#"
	1    6650 1850
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5891EA3B
P 6450 2200
F 0 "C3" H 6475 2300 50  0000 L CNN
F 1 "22pF" H 6475 2100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6488 2050 50  0001 C CNN
F 3 "" H 6450 2200 50  0000 C CNN
	1    6450 2200
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5891EB0D
P 6800 2200
F 0 "C4" H 6825 2300 50  0000 L CNN
F 1 "22pF" H 6825 2100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6838 2050 50  0001 C CNN
F 3 "" H 6800 2200 50  0000 C CNN
	1    6800 2200
	1    0    0    -1  
$EndComp
Text Label 6800 2350 3    60   ~ 0
GND
Text Label 6450 2350 3    60   ~ 0
GND
$Comp
L C C1
U 1 1 589208FF
P 1300 1150
F 0 "C1" H 1325 1250 50  0000 L CNN
F 1 "100n" H 1325 1050 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1338 1000 50  0001 C CNN
F 3 "" H 1300 1150 50  0000 C CNN
	1    1300 1150
	1    0    0    -1  
$EndComp
Text Label 1300 1300 2    60   ~ 0
GND
$Comp
L R R8
U 1 1 589236EE
P 6600 1450
F 0 "R8" V 6680 1450 50  0000 C CNN
F 1 "1M" V 6600 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6530 1450 50  0001 C CNN
F 3 "" H 6600 1450 50  0000 C CNN
	1    6600 1450
	0    1    1    0   
$EndComp
Text Label 6450 1300 1    60   ~ 0
XTAL2
Text Label 6800 1300 1    60   ~ 0
XTAL1
Text Label 2000 1450 1    60   ~ 0
AREF
Text Label 3950 950  0    60   ~ 0
S1D1
Text Label 3950 850  0    60   ~ 0
S1D2
$Comp
L SW_PUSH SW1
U 1 1 5922E1CA
P 9450 2000
F 0 "SW1" H 9600 2110 50  0000 C CNN
F 1 "SW_PUSH" H 9450 1920 50  0000 C CNN
F 2 "" H 9450 2000 50  0000 C CNN
F 3 "" H 9450 2000 50  0000 C CNN
	1    9450 2000
	1    0    0    -1  
$EndComp
Text Label 3950 1250 0    60   ~ 0
MISO
Text Label 3950 1350 0    60   ~ 0
SCK
Text Label 3950 2100 0    60   ~ 0
SDA
Text Label 3950 2200 0    60   ~ 0
SCL
Text Label 4150 3150 0    60   ~ 0
switch_buck
Text Label 4150 3050 0    60   ~ 0
switch_boost
Text Label 4150 2950 0    60   ~ 0
switch_main
Text Label 4150 1700 0    60   ~ 0
i_sense
Text GLabel 4800 1700 2    60   Input ~ 0
i_sense
Text GLabel 4800 1800 2    60   Input ~ 0
v_low_sense
Text GLabel 4800 1900 2    60   Input ~ 0
v_high_sense
Text GLabel 4850 2950 2    60   Output ~ 0
switch_main
Text GLabel 4850 3050 2    60   Output ~ 0
switch_boost
Text GLabel 4850 3150 2    60   Output ~ 0
switch_buck
Text Label 900  850  0    60   ~ 0
5V
Text Label 7100 3500 0    60   ~ 0
5V
Text GLabel 7100 3950 0    60   Output ~ 0
5V
Text Label 7500 3500 0    60   ~ 0
GND
Text GLabel 7500 3950 0    60   Output ~ 0
GND
Wire Wire Line
	2050 850  2050 950 
Wire Wire Line
	2050 3050 1800 3050
Connection ~ 2050 3050
Wire Wire Line
	2050 2950 2050 3150
Wire Wire Line
	1800 1450 2050 1450
Wire Wire Line
	2050 1150 1800 1150
Wire Wire Line
	900  850  2050 850 
Wire Wire Line
	3950 2300 4150 2300
Wire Wire Line
	3950 2200 4800 2200
Wire Wire Line
	4800 2100 3950 2100
Wire Wire Line
	3950 2000 4800 2000
Wire Wire Line
	4800 1900 3950 1900
Wire Wire Line
	3950 1800 4800 1800
Wire Wire Line
	4800 1700 3950 1700
Wire Wire Line
	3950 3150 4850 3150
Wire Wire Line
	4850 3050 3950 3050
Wire Wire Line
	3950 2950 4850 2950
Wire Wire Line
	4850 2850 3950 2850
Wire Wire Line
	3950 2750 4850 2750
Wire Wire Line
	4850 2650 3950 2650
Wire Wire Line
	3950 2550 4850 2550
Wire Wire Line
	4850 2450 3950 2450
Wire Wire Line
	3950 1350 4800 1350
Wire Wire Line
	4800 1250 3950 1250
Wire Wire Line
	4800 1150 3950 1150
Wire Wire Line
	4800 1050 3950 1050
Wire Wire Line
	4800 950  3950 950 
Wire Wire Line
	4800 850  3950 850 
Wire Wire Line
	2050 2200 1800 2200
Wire Wire Line
	2050 2300 1800 2300
Wire Wire Line
	3100 4500 2900 4500
Wire Wire Line
	3100 4600 2900 4600
Wire Wire Line
	3100 4700 2900 4700
Wire Wire Line
	3100 4900 2900 4900
Wire Notes Line
	2150 3800 2150 5200
Wire Notes Line
	2150 5200 4050 5200
Wire Notes Line
	4050 5200 4050 3800
Wire Notes Line
	4050 3800 2150 3800
Wire Wire Line
	8900 2000 9150 2000
Wire Wire Line
	9750 2000 9800 2000
Wire Wire Line
	1850 1500 1850 1450
Connection ~ 1850 1450
Wire Wire Line
	6450 1850 6500 1850
Connection ~ 6450 1850
Wire Wire Line
	6450 1300 6450 2050
Wire Wire Line
	6800 1300 6800 2050
Connection ~ 6800 1850
Wire Wire Line
	1300 1000 1300 850 
Connection ~ 1300 850 
Wire Wire Line
	6750 1450 6800 1450
Connection ~ 6800 1450
Connection ~ 6450 1450
Wire Wire Line
	7100 3500 7100 3950
Wire Wire Line
	7500 3500 7500 3950
$EndSCHEMATC
