int mainSwitchPin = 13;
int boostSwitchPin = 12;
int iPin = A0;
int vPin = A1;
int vHighPin = A5;
int iRaw = 0;
int vRaw = 0;
int vHighRaw = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(mainSwitchPin, OUTPUT);
  pinMode(boostSwitchPin, OUTPUT);
}

void loop() {
  

  // blink the boost converter:
  digitalWrite(mainSwitchPin, HIGH);
  digitalWrite(boostSwitchPin, HIGH);
  delay(100);
  // read voltage and current:
  iRaw = analogRead(iPin);
  vRaw = analogRead(vPin);
  
  vHighRaw = analogRead(vHighPin);
 
  float iValue = 1.0*((float)iRaw - 512.0)/ 1023.0 * 5.0 * 4.8;
  // 0A = 2,52V
  // 2,2A = 3,0V
  float vValue = vRaw * 5.0 / 1023 * 147/47;
  float vHighValue = vHighRaw * 5.0 / 1023 * 253/33;
  Serial.print("CurrentRaw: ");
  Serial.print(iRaw);
  Serial.print(" Current: ");
  Serial.print(iValue);
  Serial.print(" Voltage: ");
  Serial.print(vValue);
  Serial.print(" VoltageHigh: ");
  Serial.println(vHighValue);
  delay(1000);
  
//  digitalWrite(mainSwitchPin, LOW);
//  digitalWrite(boostSwitchPin, LOW);
//  delay(100);
//  // read voltage and current:
//  iRaw = analogRead(iPin);
//  vRaw = analogRead(vPin);
//  
//  iValue = 1.0*((float)iRaw - 512.0)/ 1023.0 * 5.0 * 4.8;
//  // 0A = 2,52V
//  // 2,2A = 3,0V
//  vValue = vRaw * 5.0 / 1023 * 147/47;
//  vHighValue = vHighRaw * 5.0 / 1023 * 147/47;
//  Serial.print("CurrentRaw: ");
//  Serial.print(iRaw);
//  Serial.print(" Current: ");
//  Serial.print(iValue);
//  Serial.print(" Voltage: ");
//  Serial.println(vValue);
//    Serial.print(" VoltageHigh: ");
//  Serial.println(vHighValue);
//  delay(2000);
  

}
