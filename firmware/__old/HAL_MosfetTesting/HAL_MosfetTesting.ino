//***************************
// HAL:  (see schematic for reference, Github/Gitlab)
//***************************
// - Analog:
const int vDevicePin = A1;
const int iDevicePin = A0;
const int vHomePin = A2;
const int iHome1Pin = A6;
const int iHome2Pin = A7;

// - Mosfets:
const int switchMainPin = 5;
const int switchBoostPin = 6;
const int switchBuckPin = 7;

// - Buttons:
const int push1Pin = 2;
const int push2Pin = 3;

// - LEDs;
const int LED1Pin = 8;  //D4 on PCB
const int LED2Pin = 9;  //D5 on PCB
const int LED3Pin = 10; //D6 on PCB
const int LED4Pin = 11; //D7 on PCB
const int LED5Pin = 12; //D9 on PCB

//***************************
// 
//***************************

void setup() {

  // Setting up LEDs, Mosfets & Buttons:
  pinMode(LED1Pin, OUTPUT);
  pinMode(LED2Pin, OUTPUT);
  pinMode(LED3Pin, OUTPUT);
  pinMode(LED4Pin, OUTPUT);
  pinMode(LED5Pin, OUTPUT);
  
  pinMode(switchMainPin, OUTPUT);   // ON when HIGH; OFF when LOW
  pinMode(switchBoostPin, OUTPUT);  // ON when HIGH; OFF when LOW
  pinMode(switchBuckPin, OUTPUT);   // ON when LOW, OFF; when HIGH
  
  pinMode(push1Pin, INPUT);
  pinMode(push2Pin, INPUT);


  // Start serial connection:
  Serial.begin(9600);
}

void loop() {
buck_on();

int iHome2Raw = analogRead(iHome2Pin);
Serial.print("iHome2 Raw: ");
Serial.print(iHome2Raw);

int vHomeRaw = analogRead(vHomePin);
Serial.print("  vHome Raw: ");
Serial.println(vHomeRaw);

delay(1000);
buck_off();
delay(2000);
//buck_off();
//delay(3000);
}



void boost_on(){
   digitalWrite(switchBuckPin, HIGH);  //switching BUCK-converter OFF first
   delay(10);
   digitalWrite(switchBoostPin, HIGH); //switching BOOST-converter ON 
}
void boost_off(){
   digitalWrite(switchBoostPin, LOW); //switching BOOST-converter ON 

}
void buck_on(){
   digitalWrite(switchBoostPin, LOW); //switching BOOST-converter OFF first
   delay(10);
   digitalWrite(switchBuckPin, LOW);  //switching BUCK-converter ON 
}
void buck_off(){
   digitalWrite(switchBuckPin, HIGH);  //switching BUCK-converter ON 
}
