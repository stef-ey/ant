
#include <PID_v1.h>


int pwmPin = 6;    // LED connected to digital pin 9
int vPin = A5;

double Setpoint, Input, Output;
double Kp = 1.1, Ki = 2, Kd = 0;
//double consKp=1.1, consKi=2, consKd=0;
//double aggKp=2, aggKi=4, aggKd=0;

double vConvert = 1/1024.0 * 5 * 31.7/4.7;

PID myPID(&Input, &Output, &Setpoint,2,5,1,P_ON_M, DIRECT);

void setup() {
  // nothing happens in setup
  Serial.begin(9600);
  Input = analogRead(vPin);
  Serial.print(Input);
  Serial.print("   ");
  Setpoint = 14.0/vConvert;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
}

void loop() {
 
  Setpoint = 14.0/vConvert;
  Input = analogRead(vPin);
  Serial.print(Setpoint*vConvert);
  Serial.print("  ");
  Serial.print(Input*vConvert);
  Serial.print("   ");
  double gap = abs(Setpoint-Input);
//  if(gap<10){
//    myPID.SetTunings(consKp, consKi, consKd);
 // }
//  else{
//    myPID.SetTunings(aggKp, aggKi, aggKd);
 //}
 myPID.SetTunings(Kp, Ki, Kd);
  myPID.Compute();
  analogWrite(pwmPin, Output);
  Serial.println(Output);
 
}







