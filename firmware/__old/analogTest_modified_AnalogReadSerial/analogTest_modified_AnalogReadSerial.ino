/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Graphical representation is available using serial plotter (Tools > Serial Plotter menu)
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.
*/

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A1);
  int iDevicePin = analogRead(A0);
  // print out the value you read:
  double vDevice = sensorValue / 1024.0 * 5 * 133/33; // 1024*5 gives ADC voltage. 
                                                      // From this, use voltage divider ratio to get to initial voltage: r1+r2/r2 * v_measured

  Serial.print("A1 raw = ");
  Serial.print(sensorValue);
  Serial.print(" ; --> A1 vDevice =  ");
  Serial.print(vDevice);
  Serial.print("V       ");

    Serial.print("A0 raw = ");
  Serial.print(sensorValue);
  Serial.println(" ; --> A0 iDevice =  ");

  delay(100);        // delay in between reads for stability
}
