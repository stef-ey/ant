#include <Adafruit_NeoPixel.h>

const int push1Pin = A4;
const int push2Pin = A5;

const int LEDPin = 2;


class Powerstage{
  // Handling the Powerstage: 
  // a) setting Boost or Buck-mode
  // b) setting output voltages
  // c) setting droop characteristics?
  // d) MPPT-ing PV panels
  // e) Battery charge/discharging
  // f) Load switching
  bool buckOn;
  bool boostOn;
  int vBuckSetmV;
  int vBoostSetmV;
  int vGridmV;
  int vDevicemV;
  int iGridLeftmA;
  int iGridRightmA;
  int iDevicemA;
  int mode;


  void setup(){
    // what needs to be set up?
  }
  void setMode(){
    // PV, BAT or LOAD?
    // 0 = undefined
    // 1 = PV
    // 2 = BAT
    // 3 = LOAD
    
  }
  int getCurrentMeasmA(char currentChannel){
    // left-grid, right-grid, device
  }
  int getVoltageMeasmV(char voltageChannel){
    // device or grid
  }
  void loop(){
    // what happens to the power stage in the loop?
    // depends on Mode!
    if (mode = 0){ //undefined mode
      
    }
    else if (mode = 1){ //PV mode
      
    }
    else if (mode = 2){ //BAT mode
      
    }
    else if (mode = 3){ //LOAD mode
      
    }
    else{
      //error error
    }
  }
  private:
  void boostOn(){
    
  }
  void boostOff(){
    
  }
  void buckOn(){
    
  }
  void buckOff(){
    
  }
  
};

class Button{
  // 
  const byte pin;
  int state;
  unsigned long buttonDownMs;

  public:
    Button(byte attachTo) :
      pin(attachTo)
    {
    }

    void setup() {
      pinMode(pin, INPUT);
      state = LOW;
    }

    void loop() {
      int prevState = state;
      state = digitalRead(pin);
      if (prevState == LOW && state == HIGH) {
        buttonDownMs = millis();
      }
      else if (prevState == HIGH && state == LOW) {
        if (millis() - buttonDownMs < 50) {
          // ignore this for debounce
        }
        else if (millis() - buttonDownMs < 250) {
          // short click
          //Serial.println("short");
        }
        else  {
          // long click
          //Serial.println("long");
        }
      }
    }
};


class LED{
  const int ledNumber;
  public:
    LED(int pin){
      ledNumber = pin;
    }
  void blinkLED(int cycles){
    
  }
  
  
  void setup(){
    
  }
  
  void main(){
    
  }
};

Button topButton(push1Pin);
Button bottomButton(push2Pin);
Adafruit_NeoPixel leds = Adafruit_NeoPixel(5, LEDPin, NEO_GRB + NEO_KHZ800);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  topButton.setup();
  bottomButton.setup();
  leds.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  topButton.loop();
  bottomButton.loop();

}
