
#include <Adafruit_NeoPixel.h>
#include <ADCTouch.h>
#include <SoftwareSerial.h>
#include <avr/wdt.h>

// ***************************
// HAL:  (see schematic for reference, Github/Gitlab)
// ***************************
// - Analog:

const int vDevicePin = A1;
const int iDevicePin = A0;
const int vHomePin = A2;
const int iHome1Pin = A6;
const int iHome2Pin = A7;
const int tempPin = A3;
const int NR_SAMPLES = 100;

// - sensor Calibration

const double vDevice_factor = 0.0540;
const double vDevice_summand = -0.7499;
const double iDevice_factor = 0.0248;
const double iDevice_summand = -12.822;
const double vHome_factor = 0.0502;
const double vHome_summand = 0.0202;
const double iHome1_factor = -0.0223;
const double iHome1_summand = 11.5109;
const double iHome2_factor = -0.0242;
const double iHome2_summand = 12.5258;
const double temperature_factor = 0.2504;
const double temperature_summand = -20.51;

// - Mosfets:

const int switchMainPin = 8;
const int switchBoostPin = 9;
const int switchBuckPin = 7;

// - Buttons:

const int push1Pin = A4;
const int push2Pin = A5;
const int threshButton1 = 30;
const int threshButton2 = 30;
bool Button1_pressed = false;
bool Button2_pressed = false;
const int buttondelay = 1000;

// - DC-DC Control

const int pwmBuck = 11;
const int pwmBoost = 10;
bool buckOn = true; // to not kill itself if powered from grid port
bool boostOn = false;
bool mainOn = false;

// - LEDs

const int LEDPin = 2;

// - COMMUNICATION

// CONSOLE

const int printDelay = 3000;
int consoleCount = 0;

// ***************************
// Some global variables
// ***************************

int consoleInput;
int refButton1; // for touch sensing
int refButton2; // for touch sensing
unsigned long lastprint = 0; // for printing the console
unsigned long lastButtonPress = 0;
const int iMax = 30; // the amount of times debug-loops are executed (example: BUCK BLINK, Analog value printing, ...)
int brightness = 0;

// sensor values
double vDevice = 0;
double iDevice = 0;
double vHome = 0;
double iHome1 = 0;
double iHome2 = 0;
double temp = 0;


// ***************************
// construct objects (software serial, neopixels)
// ***************************

SoftwareSerial comm(6, 5); // RX, TX
Adafruit_NeoPixel leds = Adafruit_NeoPixel(5, LEDPin, NEO_GRB + NEO_KHZ800);

void setup()
{
  // Setting up LEDs, Mosfets & Buttons:
  pinMode(LEDPin, OUTPUT);
  pinMode(switchMainPin, OUTPUT); // ON when HIGH; OFF when LOW
  pinMode(switchBoostPin, OUTPUT); // ON when HIGH; OFF when LOW
  pinMode(switchBuckPin, OUTPUT); // ON when LOW, OFF; when HIGH
  
  // Push/touch buttons
  pinMode(push1Pin, INPUT);
  pinMode(push2Pin, INPUT);
  
  // Buck/Boost control PWM pins, leave them floating for now
  pinMode(pwmBoost, INPUT);
  pinMode(pwmBoost, INPUT); //???????????????????
  
  // enable watchdog
  wdt_enable(WDTO_500MS);  //500 ms
  
  // Start serial connection:
  Serial.begin(115200);
  comm.begin(9600);
  leds.begin(); // start neopixels
  calibrateTouchbuttons();
}

void loop()
{
  // print debug-console only after several seconds (printDelay, e.g. 3 seconds) without slowing main loop
  if (millis() > lastprint + printDelay)
  {
    print_debugConsole();
    lastprint = millis();
  }
  
  // main functions in this order:
  
  measure();
  checkButtonState();
  LEDaction();
  DCDCaction();
  checkSerial();
  printSS();
  wdt_reset();
}

void print_debugConsole()
{
  consoleCount++;
  // Console prompt:
  for (int i = 0; i < 50; i++)
  {
    Serial.println(); // clear the console
  }
  Serial.println(consoleCount);
  Serial.println(F("        //       //"));
  Serial.println(F("  ___  _@@       @@_  ___"));
  Serial.println(F(" (___)(_)         (_)(___)"));
  Serial.println(F(" //|| ||           || ||||"));
  Serial.println(F("---------------------------------------------------------------------------------------------------------------------------"));
  Serial.println(F("Welcome to the Ant. Debug Console"));
  Serial.println(F("                                                  "));
  Serial.println(F("In order to do manual testing, you can select the following options (press ENTER after entering the letter"));
  Serial.println(F("CONVERTERS:"));
  Serial.println(F("m = toggle MAIN switch"));
  Serial.println(F("d = toggle BUCK(step-down)"));
  Serial.println(F("u = toggle BOOST(step-up)"));
  Serial.println(F("f = blink BUCK mode (ON/OFF every 2s)"));
  Serial.println(F("i = blink BOOST mode (ON/OFF every 2s)"));
  Serial.println(F("o = both converters OFF"));
  Serial.println(F("LEDs: press 0, 1, 2, 3, 4, 5 to turn the respective LEDs ON for 3 seconds"));
  Serial.println(F("MEASUREMENTS: a = print the current measurements (voltages/currents), l = do >a< in a loop every second"));
  Serial.println(F("BUTTONS: q = print the current state of the buttons in a loop"));
  Serial.println(F("COMMUNICATION: h = send §Hello World§ via the com port"));
  Serial.print(F("Buck State: "));
  Serial.println(buckOn);
  Serial.print(F("Boost State: "));
  Serial.println(boostOn);
  Serial.println(F("----------------------------------------------------------------------------------------------------------------------------"));
  print_analog(); // also printing the current analog values
  for (int i = 0; i < 20; i++)
  {
    Serial.println(); // make space below console
  }
}

void checkSerial()
{
  if (Serial.available() > 0)
  {
    consoleInput = Serial.read();
  }
  switch (consoleInput)
  {
    case 'd'://switching BUCK mode
    toggleBuck();
    break;
    case 'u'://switching BOOST mode
    toggleBoost();
    break;
    case 'f':
    {
      // switching to BLINK BUCK mode
      Serial.print(F("Starting to BLINK the BUCK converter for this many times: "));
      Serial.println(iMax);
      for (int i = 0; i < iMax; i++)
      {
        toggleBuck();
        delay(1000);
        toggleBuck();
        delay(1000);
        Serial.print(F(" remaining cycles: "));
        Serial.println(iMax - i);
      }
      break;
    }
    // this is weird, but if you declare a variable within the CASE, you need to use those --> {}
    case 'i':
    {
      // switchin to BLINK BOOST mode
      Serial.print(F("Starting to BLINK the BOOST converter for this many times: "));
      Serial.println(iMax);
      for (int i = 0; i < iMax; i++)
      {
        toggleBoost();
        toggleBoost();
        Serial.print(F(" remaining cycles: "));
        Serial.println(iMax - i);
      }
      break;
    }
    case 'o':
      boostOn = false;
      buckOn = false;
      Serial.println(F("switched both converters off"));
      break;
      case '0'://toggle all LEDs
      Serial.println(F("Light up all LEDs"));
      leds.setPixelColor(0, leds.Color(0, 50, 0));
      leds.setPixelColor(1, leds.Color(50, 0, 0));
      leds.setPixelColor(2, leds.Color(0, 50, 50));
      leds.setPixelColor(3, leds.Color(50, 50, 0));
      leds.setPixelColor(4, leds.Color(0, 0, 50));
      leds.show();
      delay(3000);
      // Now turn the LED off
      leds.setPixelColor(0, leds.Color(0, 0, 0));
      leds.setPixelColor(1, leds.Color(0, 0, 0));
      leds.setPixelColor(2, leds.Color(0, 0, 0));
      leds.setPixelColor(3, leds.Color(0, 0, 0));
      leds.setPixelColor(4, leds.Color(0, 0, 0));
      leds.show();
      break;
      case '1'://toggling LED 0 ff
      Serial.println(F("Light up LED 1"));
      leds.setPixelColor(0, leds.Color(0, 50, 0));
      leds.show();
      delay(3000);
      // Now turn the LED off
      // Now turn the LED off
      leds.setPixelColor(0, leds.Color(0, 0, 0));
      leds.show();
      break;
    case '2':
      Serial.println(F("Light up LED 2"));
      leds.setPixelColor(1, leds.Color(0, 50, 0));
      leds.show();
      delay(3000);
      // Now turn the LED off
      // Now turn the LED off
      leds.setPixelColor(1, leds.Color(0, 0, 0));
      leds.show();
      break;
    case '3':
      Serial.println(F("Light up LED 3"));
      leds.setPixelColor(2, leds.Color(0, 50, 0));
      leds.show();
      delay(3000);
      // Now turn the LED off
      // Now turn the LED off
      leds.setPixelColor(2, leds.Color(0, 0, 0));
      leds.show();
      break;
    case '4':
      Serial.println(F("Light up LED 4"));
      leds.setPixelColor(3, leds.Color(0, 50, 0));
      leds.show();
      delay(3000);
      // Now turn the LED off
      // Now turn the LED off
      leds.setPixelColor(3, leds.Color(0, 0, 0));
      leds.show();
      break;
    case '5':
      Serial.println(F("Light up LED 5"));
      leds.setPixelColor(4, leds.Color(0, 50, 0));
      leds.show();
      delay(3000);
      // Now turn the LED off
      // Now turn the LED off
      leds.setPixelColor(4, leds.Color(0, 0, 0));
      leds.show();
      break;
      case 'a':// printing analogue values
      print_analog();
      break;
    case 'l':
    {
      // loop-printing analog values
      for (int i = 0; i < iMax; i++)
      {
        measure();
        print_analog();
        Serial.print(F(" remaining cycles: "));
        Serial.println(iMax - i);
        delay(1000);
      }
    }
    break;
    case 'q':
    {
      // printing button-values
      for (int i = 0; i < iMax; i++)
      {
        Serial.print(F(" Button states - SW1 = "));
        Serial.print(Button1_pressed);
        Serial.print(F(" , SW2 = "));
        Serial.println(Button2_pressed);
        Serial.print(F(" ref values - SW1 = "));
        Serial.print(refButton1);
        Serial.print(F(" , SW2 = "));
        Serial.println(refButton2);
        Serial.print(F(" actual values - SW1 = "));
        Serial.print(ADCTouch.read(push1Pin));
        Serial.print(F(" , SW2 = "));
        Serial.println(ADCTouch.read(push2Pin));
        Serial.println(F("########################"));
        checkButtonState();
        LEDaction();
        delay(300);
      }
      break;
      case 'm':
        toggleMain();
        break;
      case 'h':
        Serial.println(F("sending §hello world§ to com port"));
        comm.println("hello world");
        break;
    }
  }
}

void print_analog()
{
  Serial.print(F("ANALOG VALUES: "));
  Serial.print(F("vDevice = "));
  Serial.print(vDevice);
  Serial.print(F(" , iDevice = "));
  Serial.print(iDevice);
  Serial.print(F(" , vHome = "));
  Serial.print(vHome);
  Serial.print(F(" , iHome1 = "));
  Serial.print(iHome1);
  Serial.print(F(" , iHome2 = "));
  Serial.print(iHome2);
  Serial.print(F(" , Temperature = "));
  Serial.println(temp);
}

void calibrateTouchbuttons()
{
  refButton1 = ADCTouch.read(push1Pin, 500);
  refButton2 = ADCTouch.read(push2Pin, 500);
}

void checkButtonState()
{
  if (millis() > lastButtonPress + buttondelay)
  {
    int temp1 = ADCTouch.read(push1Pin);
    int temp2 = ADCTouch.read(push2Pin);
    temp1 = temp1 - refButton1;
    temp2 = temp2 - refButton2;
    if (temp1 > threshButton1)
    {
      Button1_pressed = true;
      toggleMain();
      //toggleBuck();
      lastButtonPress = millis();
    }
    else
    {
      Button1_pressed = false;
    }
    if (temp2 > threshButton2)
    {
      Button2_pressed = true;
      mainOn = true;
      toggleBoost();
      lastButtonPress = millis();
    }
    else
    {
      Button2_pressed = false;
    }
  }
}

void LEDaction()
{
  if (buckOn & mainOn)
  {
      leds.setPixelColor(0, leds.Color(0, brightness, 0));
      leds.setPixelColor(1, leds.Color(brightness, brightness, 0));
      leds.setPixelColor(2, leds.Color(brightness, 0, 0));
      brightness = brightness + 2;
      if (brightness >= 50){
        brightness = 0;
      
    }

    
  }
  else
  {
    leds.setPixelColor(0, leds.Color(0, 0, 0));
    leds.setPixelColor(1, leds.Color(0, 0, 0));
    leds.setPixelColor(2, leds.Color(0, 0, 0));
  }
  if (boostOn)
  {
    leds.setPixelColor(3, leds.Color(0, 0, 255));
    leds.setPixelColor(4, leds.Color(0, 0, 255));
  }
  else
  {
    leds.setPixelColor(3, leds.Color(0, 0, 0));
    leds.setPixelColor(4, leds.Color(0, 0, 0));
  }
  leds.show();
}

void DCDCaction()
{
  if (buckOn)
  {
    boostOn = false;
    digitalWrite(switchBoostPin, LOW); // switching BOOST-converter OFF first
    delay(10);
    digitalWrite(switchBuckPin, LOW); // switching BUCK-converter ON
  }
  else
  {
    digitalWrite(switchBuckPin, HIGH); // switching BUCK-converter off
  }
  if (boostOn)
  {
    buckOn = false;
    digitalWrite(switchBuckPin, HIGH); // switching BUCK-converter OFF first
    delay(10);
    digitalWrite(switchBoostPin, HIGH); // switching BOOST-converter ON
  }
  else
  {
    digitalWrite(switchBoostPin, LOW); // switching BOOST-converter OFF
  }
  if (mainOn)
  {
    digitalWrite(switchMainPin, HIGH); // switching Main switch on
  }
  else
  {
    digitalWrite(switchMainPin, LOW); // switching Main switch off
  }
}

void toggleBuck()
{
  if (buckOn)
  {
    buckOn = false;
    Serial.println(F("I switched the BUCK converter OFF"));
  }
  else
  {
    buckOn = true;
    boostOn = false;
    Serial.println(F("I switched the BUCK converter ON"));
  }
}

void toggleBoost()
{
  if (boostOn)
  {
    boostOn = false;
    Serial.println(F("I switched the boost converter OFF"));
  }
  else
  {
    boostOn = true;
    buckOn = false;
    Serial.println(F("I switched the boost converter ON"));
  }
}

void toggleMain()
{
  if (mainOn)
  {
    mainOn = false;
    Serial.println(F("I switched the MAIN switch OFF"));
  }
  else
  {
    mainOn = true;
    Serial.println(F("I switched the MAIN switch ON"));
  }
}

void printSS()
{
  // print everything that is being recieved on the com line
  if (comm.available())
  {
    Serial.print(F("RECIEVED VIA COM: "));
    Serial.write(comm.read());
    Serial.println();
  }
}

void measure()
{
  vDevice = get_value(vDevicePin, vDevice_factor, vDevice_summand);
  iDevice = get_value(iDevicePin, iDevice_factor, iDevice_summand);
  vHome = get_value(vHomePin, vHome_factor, vHome_summand);
  iHome1 = get_value(iHome1Pin, iHome1_factor, iHome1_summand);
  iHome2 = get_value(iHome2Pin, iHome2_factor, iHome2_summand);
  temp = get_value(tempPin, temperature_factor, temperature_summand);
}

double get_value(int sensorPin, double factor, double summand)
{
  // take one value every timestep and summ it up, divide it
  double value = 0;
  for (int i = 0; i < NR_SAMPLES; i++)
  {
    value = value + analogRead(sensorPin);
    delayMicroseconds(50);
  }
  value = value / NR_SAMPLES;

  // transform it to actual value
  double transformed_value = (value * factor) + summand;
  return transformed_value;
}
