int mainSwitchPin = 13;
int boostSwitchPin = 12;
int iPin = A0;
int vPin = A1;
int iRaw = 0;
int vRaw = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(mainSwitchPin, OUTPUT);
  pinMode(boostSwitchPin, OUTPUT);
}

void loop() {
  

  // blink the boost converter:
  digitalWrite(mainSwitchPin, HIGH);
  digitalWrite(boostSwitchPin, HIGH);
  // read voltage and current:
  iRaw = analogRead(iPin);
  vRaw = analogRead(vPin);
  float iValue = (iRaw / 512.0 - 1.0) * 2.5 / 2  * 20;
  float vValue = vRaw * 5.0 / 1023 * 147/47;
  Serial.print("Current: ");
  Serial.print(iValue);
  Serial.print(" Voltage: ");
  Serial.println(vValue);
  delay(1000);
  
  digitalWrite(mainSwitchPin, LOW);
  digitalWrite(boostSwitchPin, LOW);
  // read voltage and current:
  iRaw = analogRead(iPin);
  vRaw = analogRead(vPin);
  iValue = (iRaw / 512.0 - 1.0) * 2.5 / 2  * 20;
  vValue = vRaw * 5.0 / 1023 * 147/47;
  Serial.print("Current: ");
  Serial.print(iValue);
  Serial.print(" Voltage: ");
  Serial.println(vValue);
  delay(1000);
  

}
