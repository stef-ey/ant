/*
  DigitalReadSerial
 Reads a digital input on pin 2, prints the result to the serial monitor

 This example code is in the public domain.
 */

// digital pin 2 has a pushbutton attached to it. Give it a name:
int push1 = 2;
int push2 = 3;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(push1, INPUT);
  pinMode(push2, INPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int buttonState1 = digitalRead(push1);
  int buttonState2 = digitalRead(push2);
  // print out the state of the button:
  Serial.print(buttonState1);
  Serial.print("   ;   ");
  Serial.println(buttonState2);
  delay(100);        // delay in between reads for stability
}



