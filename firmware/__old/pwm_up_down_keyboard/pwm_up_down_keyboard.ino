/*
 Fading

 This example shows how to fade an LED using the analogWrite() function.

 The circuit:
 * LED attached from digital pin 9 to ground.

 Created 1 Nov 2008
 By David A. Mellis
 modified 30 Aug 2011
 By Tom Igoe

 http://www.arduino.cc/en/Tutorial/Fading

 This example code is in the public domain.

 */


int ledPin = 6;    // LED connected to digital pin 9
int vPin = A5;
int fadeValue = 125;
int consoleInput;
int vSetpoint = 29000; //in mV
int vRaw;
double vReadable; //in mV
double vConvert = 1/1024.0 * 5 * 57.2/4.7*1000;

void setup() {
  // nothing happens in setup
  Serial.begin(115200);
  
}

void loop() {


  if (Serial.available() > 0) {
    consoleInput = Serial.read();
  }
  
   Serial.print(consoleInput);
   
   switch (consoleInput) {
    case 'w': //stepping down
      vSetpoint = vSetpoint+100;
    break;
    case 's': //stepping down
      vSetpoint = vSetpoint-100;
    break;
    case 'd': //stepping down
      vSetpoint = vSetpoint+1000;
    break;
    case 'a': //stepping down
      vSetpoint = vSetpoint-1000;
    break;
   }   
   Serial.print(" vSetpoint: ");
   Serial.print(vSetpoint);

   vRaw = analogRead(vPin);
   vReadable = vRaw * 1.0 * vConvert;

   Serial.print(" vRaw = ");
   Serial.print(vRaw);
   Serial.print(" vReadable = ");
   Serial.print(vReadable);

   if(vReadable<vSetpoint){
    fadeValue--;

   }
   if(vReadable>vSetpoint){
    fadeValue++;
   }
       if(fadeValue>255){
      fadeValue=255;
    }
        if(fadeValue<0){
      fadeValue=0;
    }
   Serial.print(" fadeValue = ");
   Serial.println(fadeValue);
   
   analogWrite(ledPin, fadeValue);
   // wait for 30 milliseconds to see the dimming effect
   delay(60);
  

}


